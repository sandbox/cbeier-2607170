<?php

/**
 * @file
 * Contains \Drupal\editor\Form\EditorImageDialog.
 */

namespace Drupal\ckeditor_image_style\Form;

use Drupal\editor\Form\EditorImageDialog;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;


/**
 * Provides an image dialog for text editors.
 */
class CkeditorImageStyleImageDialog extends EditorImageDialog {

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\filter\Entity\FilterFormat $filter_format
   *   The filter format for which this dialog corresponds.
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    $form = parent::buildForm($form, $form_state, $filter_format);

    if (isset($form_state->getUserInput()['editor_object'])) {
      // By convention, the data that the text editor sends to any dialog is in
      // the 'editor_object' key. And the image dialog for text editors expects
      // that data to be the attributes for an <img> element.
      $image_element = $form_state->getUserInput()['editor_object'];
    }
    else {
      // Retrieve the image element's attributes from form state.
      $image_element = $form_state->get('image_element') ?: [];
    }

    // Get the image style, if a image is already placed in the editor.
    preg_match('/styles\/([a-z0-9]+)\//', $image_element['src'], $matches);
    if (isset($matches[1])) {
      $style = $matches[1];
    }

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => t('Image style'),
      '#default_value' => isset($style) ? $style : '',
      '#options' => image_style_options(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = parent::submitForm($form, $form_state);

    $fid = $form_state->getValue(array('fid', 0));
    $image_style = $form_state->getValue('image_style');
    if (!empty($fid) && !empty($image_style)) {
      $file = $this->fileStorage->load($fid);
      $image_style = ImageStyle::load($image_style);

      $file_url = $image_style->buildUrl($file->getFileUri());

      // Transform absolute image URLs to relative image URLs: prevent problems
      // on multisite set-ups and prevent mixed content errors.
      $file_url = file_url_transform_relative($file_url);

      $form_state->setValue(array('attributes', 'src'), $file_url);
    }

    if (!$form_state->getErrors()) {
      $response = new AjaxResponse();
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }
}
