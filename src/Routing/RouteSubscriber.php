<?php
/**
 * @file
 * Contains \Drupal\ckeditor_image_style\Routing\RouteSubscriber.
 */

namespace Drupal\ckeditor_image_style\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Change the form class of the editor image_dialog form.
    if ($route = $collection->get('editor.image_dialog')) {
      $route->setDefault('_form', '\Drupal\ckeditor_image_style\Form\CkeditorImageStyleImageDialog');
    }
  }

}
